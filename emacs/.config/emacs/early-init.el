(setq package-enable-at-startup nil
      inhibit-startup-message t
      native-comp-async-report-warnings-errors 'silent)

(defun find-libemutls-path-and-setenv ()
  "Find the path to libemutls_w.a and set LIBRARY_PATH environment variable if OS is macOS."
  (when (eq system-type 'darwin)
    (let* ((lib-path (shell-command-to-string "find /opt/homebrew -name libemutls_w.a 2>/dev/null | head -n 1"))
           (dir-path (when (not (string= lib-path ""))
                       (string-trim (shell-command-to-string (format "dirname %s" lib-path))))))
      (if dir-path
          (progn
            (setenv "LIBRARY_PATH" dir-path)
            (message "LIBRARY_PATH set to: %s" dir-path))
        (message "libemutls_w.a not found")))))

(find-libemutls-path-and-setenv)

(setenv "LSP_USE_PLISTS" "true")
