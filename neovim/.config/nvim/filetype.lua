vim.filetype.add({
    extension = {
        h = function(_,_)
            if vim.fn.search("\\C^#include <[^>.]\\+>$", "nw") ~= 0 then
                return "cpp"
            end
            return "c"
        end,
        yml = "yaml.ansible",
        tf = "terraform"
    }
})
