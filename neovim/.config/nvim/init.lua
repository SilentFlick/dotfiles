local loader = vim.loader
loader.enable()

require("options")
require("keymaps")
require("autocmd")
require("plugins")
