local opt_local = vim.opt_local
local api = vim.api
local cmd = vim.cmd
local fn = vim.fn

local function autocmd(n, e, p, cb)
    local group = api.nvim_create_augroup(n, {clear = false})
    api.nvim_create_autocmd(e, {
        group = group,
        pattern = p,
        callback = cb
    })
end

autocmd("highlight", "TextYankPost", "*", function()
    vim.highlight.on_yank({higroup = "Visual", timeout = 120})
end)
autocmd("terminal", "TermOpen", "*", function ()
    opt_local.number = false
    opt_local.relativenumber = false
    opt_local.spell = false
    cmd.startinsert()
end)
autocmd("position", "BufReadPost", "*", function ()
    fn.execute 'normal! g`"'
end)
