local o = vim.o
local opt = vim.opt
local g = vim.g

o.background = "light"
o.timeoutlen = 500
o.updatetime = 50
o.scrolloff = 8
o.number = true
o.numberwidth = 2
o.relativenumber = true
o.signcolumn = "yes"
o.expandtab = true
o.smarttab = true
o.cindent = true
o.autoindent = true
o.wrap = true
o.textwidth = 0
o.wrapmargin = 0
o.tabstop = 4
o.shiftwidth = 4
o.softtabstop = -1
o.list = true
o.listchars = "trail:·,nbsp:◇,tab:→ ,extends:▸,precedes:◂,multispace:···⬝,leadmultispace:│   ,"
o.clipboard = "unnamedplus"
o.ignorecase = true
o.smartcase = true
o.backup = false
o.writebackup = false
o.undofile = true
o.swapfile = false
o.history = 50
o.splitright = true
o.splitbelow = true
o.foldmethod = "indent"
o.foldenable = false
o.foldlevelstart = 99
o.foldnestmax = 3
o.foldminlines = 1
o.linebreak = true
o.spell = true

opt.mouse = "a"
opt.laststatus = 3

opt.colorcolumn = "120"
o.cursorline = true

g.mapleader = " "
g.maplocalleader = " "

function ChangeBackground ()
    local theme = vim.fn.system({"defaults", "read", "-g", "AppleInterfaceStyle"})
    theme = vim.fn.trim(theme)
    if theme == "Dark" then
        vim.o.background = "dark"
    else
        vim.o.background = "light"
    end
end
ChangeBackground()
