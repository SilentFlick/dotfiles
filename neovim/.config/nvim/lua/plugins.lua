local fn = vim.fn

local install_path = fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(install_path) then
    fn.system({
        "git", "clone", "--filter=blob:none", "--branch=stable",
        "https://github.com/folke/lazy.nvim.git", install_path
    })
end
vim.opt.rtp:prepend(install_path)

require("lazy").setup({
    require("plugins.ui"),
    require("plugins.ide"),
    require("plugins.tools")
})
