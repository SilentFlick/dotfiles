return ({
    {
        "numToStr/Comment.nvim",
        event = "BufRead",
        config = function() require("Comment").setup() end,
        lazy = true
    },
    {
        "nvim-treesitter/nvim-treesitter",
        config = function()
            require("nvim-treesitter.configs").setup {
                sync_install = false,
                auto_install = true,
                highlight = {
                    enable = true
                },
                ensure_installed = {
                    "bash", "c", "lua", "comment", "dockerfile", "html", "javascript", "json", "latex", "make", "markdown",
                    "python", "regex", "toml", "vim", "yaml"
                }
            }
        end,
        lazy = false
    },
    {
        "neovim/nvim-lspconfig",
        event = { "BufReadPost", "BufNewFile" },
        lazy = true,
        cmd = { "LspInfo", "LspInstall", "LspUninstall" },
        dependencies = {
            "folke/neodev.nvim",
            "williamboman/mason.nvim",
            "williamboman/mason-lspconfig.nvim",
            "hrsh7th/nvim-cmp",
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-nvim-lsp-signature-help",
            "saadparwaiz1/cmp_luasnip",
            "L3MON4D3/LuaSnip",
            "onsails/lspkind.nvim"
        },
        config = function()
            local mason = require("mason")
            local mason_lspconfig = require("mason-lspconfig")
            local lspconfig = require("lspconfig")
            local capabilities = require("cmp_nvim_lsp").default_capabilities()
            local cmp = require("cmp")

            cmp.setup({
                snippet = {
                    expand = function(args)
                        require("luasnip").lsp_expand(args.body)
                    end,
                },
                mapping = cmp.mapping.preset.insert({
                    ["<C-u>"] = cmp.mapping.scroll_docs(-4),
                    ["<C-d>"] = cmp.mapping.scroll_docs(4),
                    ["<C-Space>"] = cmp.mapping.complete(),
                    ["<C-e>"] = cmp.mapping.abort(),
                    ["<CR>"] = cmp.mapping.confirm({select = true})
                }),
                formatting = {
                    format = require("lspkind").cmp_format({
                        mode = "symbol",
                        maxwidth = 50,
                        show_labelDetails = true,
                        before = function(_, vim_item)
                            return vim_item
                        end
                    })
                },
                window = {
                    completion = cmp.config.window.bordered(),
                    documentation = cmp.config.window.bordered()
                },
                sources = {
                    {name = "nvim_lsp"}, {name = "luasnip"}
                },
                experimental = {
                    native_menu = false,
                    ghost_text = true
                }
            })
            mason.setup()
            mason_lspconfig.setup({
                ensure_installed = {},
                automatic_installation = true,
                handlers = {
                    function(server_name)
                        lspconfig[server_name].setup {
                            capabilities = capabilities,
                            autostart = true,
                            settings = {
                                Lua = {
                                    diagnostics = {
                                        globals = {"vim"}
                                    }
                                }
                            }
                        }
                    end
                }
            })
        end
    }
})
