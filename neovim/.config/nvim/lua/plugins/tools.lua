return ({
    {
        "nvim-telescope/telescope.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim"
        },
        config = function()
            local builtin = require("telescope.builtin")
            map("n", "<Leader>of", ":Telescope<CR>")
            map("n", "<Leader>ff", builtin.find_files)
            map("n", "<Leader>fg", builtin.live_grep)
            map("n", "<Leader>fb", builtin.buffers)
            map("n", "<Leader>fh", builtin.help_tags)
        end
    },
    {
        "nvim-telescope/telescope-bibtex.nvim",
        dependencies = {
            "nvim-telescope/telescope.nvim"
        },
        config = function ()
            local telescope = require("telescope")
            telescope.setup {
                extensions = {
                    bibtex = {
                        global_files = { "~/Nextcloud/Projects/Org/books.bib" },
                        search_keys = { "author", "year", "title", "label" },
                    }
                }
            }
            telescope.load_extension("bibtex")
            map("n", "<Leader>fn", ":Telescope bibtex<CR>")
        end
    },
    {
        "nvim-telescope/telescope-file-browser.nvim",
        dependencies = {
            "nvim-telescope/telescope.nvim",
            "nvim-lua/plenary.nvim"
        },
        config = function ()
            local telescope = require("telescope")
            telescope.load_extension("file_browser")
            map("n", "<Leader>fc", ":Telescope file_browser path=%:p:h select_buffer=true<CR>")
        end
    }
})
