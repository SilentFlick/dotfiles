if [ -f ~/.viminfo ]; then
    rm ~/.viminfo
fi
if [ -f ~/.fehbg ]; then
    rm ~/.fehbg
fi

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
        source "$HOME/.bashrc"
    fi
fi

if [ -z "${DISPLAY}" ] && [ "$(tty)" = "/dev/tty1" ]; then
    #exec startx "$XDG_CONFIG_HOME/X11/xinitrc"
    exec startx "$XDG_CONFIG_HOME/X11/xinitrc" -- -keeptty >~/.local/share/xorg/xorg.log 2>&1
fi

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/Users/tanminhho/.sdkman"
[[ -s "/Users/tanminhho/.sdkman/bin/sdkman-init.sh" ]] && source "/Users/tanminhho/.sdkman/bin/sdkman-init.sh"
