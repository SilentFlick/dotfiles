if [ -f "$HOME/.shell-aliases" ]; then
    source "$HOME/.shell-aliases"
fi
[[ "$-" != *i* ]] && return
export HISTCONTROL=ignoreboth:erasedups
export EDITOR='emacsclient'
export VISUAL='nvim'
export BAT_THEME="gruvbox-light"
if which bat >/dev/null 2>&1; then
    export MANPAGER="sh -c 'col -bx | bat -l man -p'"
    export MANROFFOPT="-c"
else
    export MANPAGER="less -R --use-color -Dd+r -Du+b"
fi
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export PYTHONSTARTUP="$XDG_CONFIG_HOME/python/startup"
export HISTFILE="$XDG_DATA_HOME/bash/history"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export WGETRC="$XDG_CONFIG_HOME/wget/rc"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export GOPATH="$XDG_DATA_HOME/go"
export DOCKER_CONFIG="$XDG_CONFIG_HOME"/docker
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export TEXMFVAR="$XDG_CACHE_HOME"/texlive/texmf-var
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority
export INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc
export PYTHONSTARTUP="${XDG_CONFIG_HOME}/python/pythonrc"
export SQLITE_HISTORY="$XDG_CACHE_HOME"/sqlite_history
export GPG_TTY=$(tty)
if [ -d "$HOME/.bin" ]; then
    PATH="$HOME/.bin:$PATH"
fi
if [ -d "$HOME/.local/bin" ]; then
    PATH="$HOME/.local/bin:$PATH"
fi
if [ -d "$HOME/.emacs.d/bin" ]; then
    PATH="$PATH:$HOME/.emacs.d/bin"
fi
if [ -d "$HOME/.local/share/npm/bin/" ]; then
    PATH="$PATH:$HOME/.local/share/npm/bin/"
fi
if [ -d "$HOME/.local/share/go/bin/" ]; then
    PATH="$PATH:$HOME/.local/share/go/bin/"
fi
if [ -d "$HOME/.local/share/JetBrains/Toolbox/scripts/" ]; then
    PATH="$PATH:$HOME/.local/share/JetBrains/Toolbox/scripts"
fi
#ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"
#shopt
shopt -s autocd  # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s dotglob
shopt -s checkwinsize
shopt -s expand_aliases # expand aliases
# Install pkgfile and enable pkgfile-update.timer
if [ -r /usr/share/doc/pkgfile/command-not-found.bash ]; then
    source /usr/share/doc/pkgfile/command-not-found.bash
fi
#bash autocomplete
if [ -r /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
fi
[[ $TERM = "dumb" ]] && PS1='$ '
[[ $TERM = "tramp" ]] && PS1='$ '

eval "$(starship init bash)"

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
