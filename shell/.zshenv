export HISTCONTROL=ignoreboth:erasedups
export EDITOR="nvim"
export VISUAL="nvim"
export MANPAGER="less -R --use-color -Dd+r -Du+b"

# $HOME cleanup
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export PYTHONSTARTUP="$XDG_CONFIG_HOME/python/startup"
export HISTFILE="$XDG_DATA_HOME/bash/history"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export WGETRC="$XDG_CONFIG_HOME/wget/rc"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export GOPATH="$XDG_DATA_HOME/go"
export DOCKER_CONFIG="$XDG_CONFIG_HOME"/docker
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export TEXMFVAR="$XDG_CACHE_HOME"/texlive/texmf-var
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority
export INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc
export PYTHONSTARTUP="${XDG_CONFIG_HOME}/python/pythonrc"
export SQLITE_HISTORY="$XDG_CACHE_HOME"/sqlite_history
export GPG_TTY=$(tty)
export LDFLAGS="-L/opt/homebrew/opt/node@20/lib"
export CPPFLAGS="-I/opt/homebrew/opt/node@20/include"

function pathappend {
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
        PATH="${PATH:+"$PATH:"}$1"
    fi
}

function pathpush {
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
        PATH="$1${PATH:+":$PATH"}"
    fi
}

pathappend "$HOME/.bin"
pathappend "$HOME/.local/bin"
pathappend "$HOME/.config/npm/bin"
pathappend "$GOPATH/bin"
pathappend "/opt/homebrew/bin"
pathappend "/opt/homebrew/opt/util-linux/bin"
pathappend "/opt/homebrew/opt/util-linux/sbin"
pathpush "/opt/homebrew/opt/python/libexec/bin"
export PATH
